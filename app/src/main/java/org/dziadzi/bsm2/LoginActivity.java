package org.dziadzi.bsm2;

import android.app.KeyguardManager;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.dziadzi.bsm2.security.AuthenticationProvider;
import org.dziadzi.bsm2.security.FingerprintException;
import org.dziadzi.bsm2.security.FingerprintHandler;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static android.security.keystore.KeyProperties.*;

public class LoginActivity extends AppCompatActivity {


    private static final String KEY_NAME = "keyName";
    private AuthenticationProvider authenticationProvider;
    private TextView message;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        authenticationProvider = new AuthenticationProvider(this);
        Button logIn = (Button) findViewById(R.id.logIn);
        logIn.setEnabled(false);
        Button register = (Button) findViewById(R.id.registerButton);
        register.setEnabled(false);
        final TextView name = (TextView) findViewById(R.id.name);
        final TextView password = (TextView) findViewById(R.id.password);
        name.setEnabled(false);
        password.setEnabled(false);
        initializeFingerprint(logIn, name, password, register);
        logIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Boolean authenticated = authenticationProvider.authenticate(name.getText().toString(), password.getText().toString());
                if(authenticated){
                    Intent intent = new Intent(LoginActivity.this, PasswordChangeActivity.class);
                    intent.putExtra("secret",authenticationProvider.getSecret(name.getText().toString(),password.getText().toString()));
                    intent.putExtra("login",name.getText().toString());
                    startActivity(intent);
                }


                Log.v("Authentication result", authenticated.toString());
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
            }
        });
    }

    protected void initializeFingerprint(Button logIn, TextView name, TextView password, Button register) {

        message = (TextView) findViewById(R.id.fingerStatus);
        Button btn = (Button) findViewById(R.id.fingerBtn);

        final FingerprintHandler fph = new FingerprintHandler(logIn, name, password, register);

        if (!checkFinger()) {
            btn.setEnabled(false);
        }
        else {
            // We are ready to set up the cipher and the key
            try {
                generateKey();
                Cipher cipher = generateCipher();
                cryptoObject =
                        new FingerprintManager.CryptoObject(cipher);
            }
            catch(FingerprintException fpe) {
                // Handle exception
                btn.setEnabled(false);
            }
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                message.setText("Przesuń palec po czytniku");
                fph.doAuth(fingerprintManager, cryptoObject);
            }
        });
    }



    private boolean checkFinger() {

        // Keyguard Manager
        KeyguardManager keyguardManager = (KeyguardManager)
                getSystemService(KEYGUARD_SERVICE);

        // Fingerprint Manager
        fingerprintManager = (FingerprintManager)
                getSystemService(FINGERPRINT_SERVICE);

        try {
            // Check if the fingerprint sensor is present
            if (!fingerprintManager.isHardwareDetected()) {
                // Update the UI with a message
                message.setText("Fingerprint authentication not supported");
                return false;
            }

            if (!fingerprintManager.hasEnrolledFingerprints()) {
                message.setText("No fingerprint configured.");
                return false;
            }

            if (!keyguardManager.isKeyguardSecure()) {
                message.setText("Secure lock screen not enabled");
                return false;
            }
        }
        catch(SecurityException se) {
            se.printStackTrace();
        }
        return true;
    }

    private void generateKey() throws FingerprintException {
        try {
            // Get the reference to the key store
            keyStore = KeyStore.getInstance("AndroidKeyStore");

            // Key generator to generate the key
            keyGenerator = KeyGenerator.getInstance(KEY_ALGORITHM_AES,

                    "AndroidKeyStore");

            keyStore.load(null);
            keyGenerator.init( new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    PURPOSE_ENCRYPT |
                            PURPOSE_DECRYPT)
                    .setBlockModes(BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            ENCRYPTION_PADDING_PKCS7)
                    .build());

            keyGenerator.generateKey();
        }
        catch(KeyStoreException
                | NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException
                | CertificateException
                | IOException exc) {
            exc.printStackTrace();
            throw new FingerprintException(exc);
        }
    }

    private Cipher generateCipher() throws FingerprintException {
        try {
            Cipher cipher = Cipher.getInstance(KEY_ALGORITHM_AES + "/"
                    + BLOCK_MODE_CBC + "/"
                    + ENCRYPTION_PADDING_PKCS7);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return cipher;
        }
        catch (NoSuchAlgorithmException
                | NoSuchPaddingException
                | InvalidKeyException
                | UnrecoverableKeyException
                | KeyStoreException exc) {
            exc.printStackTrace();
            throw new FingerprintException(exc);
        }
    }

}

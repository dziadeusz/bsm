package org.dziadzi.bsm2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.dziadzi.bsm2.security.RegistrationService;

public class PasswordChangeActivity extends AppCompatActivity {

    private RegistrationService registrationService;
    private String whizzleWhizzle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change);
        registrationService = new RegistrationService(this);
        final Button passwordChange = (Button) findViewById(R.id.passwordChangeButton);
        final TextView secret = (TextView) findViewById(R.id.secret);
        Intent intent = getIntent();
        String easyPuzzle = intent.getExtras().getString("secret");
        this.whizzleWhizzle = intent.getExtras().getString("login");
        secret.setText(easyPuzzle);
        final TextView password = (TextView) findViewById(R.id.password);
        final TextView passwordConfirmation = (TextView) findViewById(R.id.password_confirmation);

        passwordChange.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(password.getText().toString().equals(passwordConfirmation.getText().toString())){

                    registrationService.eraseToken(whizzleWhizzle);
                    Boolean registered = registrationService.register(whizzleWhizzle,secret.getText().toString(), password.getText().toString(), passwordConfirmation.getText().toString());
                startActivity(new Intent(PasswordChangeActivity.this,LoginActivity.class));
                }



            }
        });
    }
}

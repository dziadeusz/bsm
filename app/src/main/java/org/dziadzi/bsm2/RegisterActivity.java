package org.dziadzi.bsm2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.dziadzi.bsm2.security.RegistrationService;

public class RegisterActivity extends AppCompatActivity {

    private RegistrationService registrationService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        registrationService = new RegistrationService(this);
        final Button register = (Button) findViewById(R.id.registerButton);
        final TextView name = (TextView) findViewById(R.id.name);
        final TextView secret = (TextView) findViewById(R.id.secret);
        final TextView password = (TextView) findViewById(R.id.password);
        final TextView passwordConfirmation = (TextView) findViewById(R.id.password_confirmation);

     register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Boolean registered = registrationService.register(name.getText().toString(),secret.getText().toString(), password.getText().toString(), passwordConfirmation.getText().toString());
                if(registered){
                    startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                }


                Log.v("Authentication result", registered.toString());
            }
        });
    }
}

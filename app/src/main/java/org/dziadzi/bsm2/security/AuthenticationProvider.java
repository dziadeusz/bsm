package org.dziadzi.bsm2.security;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import javax.crypto.BadPaddingException;

/**
 * Created by DELL on 2016-05-19.
 */
public class AuthenticationProvider {


    private final Context context;
    private final TokenHolder tokenHolder;
    private Encryptor encryptor = new Encryptor();

    public AuthenticationProvider(Context context) {
        this.context = context;
        this.tokenHolder = new TokenHolder(context);
    }

    public Boolean authenticate(String username, String password) {

        boolean authenticated = false;
        if(!tokenHolder.hasToken(username)){
            return authenticated;
        }
        String accesToken = Base64.encodeToString(username.getBytes(), Base64.NO_WRAP);
        String login = tokenHolder.getToken(accesToken).getAccessToken();
        try{
        if(encryptor.decrypt(context,login,password).equals(username)){
            authenticated=true;
        }}catch (Exception e){
            Log.v("Wrong ","password");
        }


        return authenticated;
    }
    public String getSecret(String username, String password){
        return encryptor.decrypt(context,tokenHolder.getToken(username).getAccessToken(),password);
    }
    public Token getToken(String accesToken) {
        return tokenHolder.getToken(accesToken);
    }
}

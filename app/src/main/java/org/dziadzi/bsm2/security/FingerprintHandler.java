package org.dziadzi.bsm2.security;

import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by dziadeusz on 05.12.16.
 */
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {
    private final Button login;
    private final TextView name;
    private final TextView password;
    private final Button register;

    public FingerprintHandler(Button logIn, TextView name, TextView password, Button register) {
        this.login=logIn;
        this.name=name;
        this.password=password;
        this.register=register;
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        super.onAuthenticationError(errorCode, errString);

    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        super.onAuthenticationHelp(helpCode, helpString);
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        super.onAuthenticationSucceeded(result);
        login.setEnabled(true);
        name.setEnabled(true);
        password.setEnabled(true);
        register.setEnabled(true);
    }

    @Override
    public void onAuthenticationFailed() {
        super.onAuthenticationFailed();
    }

    public void doAuth(FingerprintManager manager,
                       FingerprintManager.CryptoObject obj) {
        CancellationSignal signal = new CancellationSignal();

        try {
            manager.authenticate(obj, signal, 0, this, null);
        }
        catch(SecurityException sce) {}
    }
}
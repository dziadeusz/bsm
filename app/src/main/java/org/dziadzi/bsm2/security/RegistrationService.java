package org.dziadzi.bsm2.security;

import android.content.Context;

/**
 * Created by DELL on 2016-05-19.
 */
public class RegistrationService {


    private final Context context;
    private final TokenHolder tokenHolder;
    private Encryptor encryptor = new Encryptor();

    public RegistrationService(Context context) {
        this.context= context;
        this.tokenHolder = new TokenHolder(context);
    }

    public Boolean register(String login,String secret, String password, String passwordConfirmation){
        boolean registered=false;
        boolean match = password.equals(passwordConfirmation);
        boolean notTaken = !tokenHolder.hasToken(login);
        if(match && notTaken) {
           Token token = Token.builder().withAccessToken(encryptor.encrypt(context, secret, password)).withTokenType(encryptor.encrypt(context, login, password))
                   .withRefreshToken(login).withExpiresIn("five_nanoseconds").build();
           tokenHolder.store(token);
        registered = true;
       }
        return registered;
    }

    public void eraseToken(String token) {
        tokenHolder.eraseToken(token);
    }

    public Token getToken(String token) {
        return tokenHolder.getToken(token);
    }


}

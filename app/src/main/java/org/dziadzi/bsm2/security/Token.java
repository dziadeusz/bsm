package org.dziadzi.bsm2.security;

/**
 * Created by DELL on 2016-05-19.
 */
public class Token {
    private String accessToken;
    private String tokenType;
    private String refreshToken;
    private String expiresIn;

    private Token(Builder builder) {
        setAccessToken(builder.accessToken);
        setTokenType(builder.tokenType);
        setRefreshToken(builder.refreshToken);
        setExpiresIn(builder.expiresIn);
    }

    public static IAccessToken builder() {
        return new Builder();
    }


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }

    public interface IBuild {
        Token build();
    }

    public interface IExpiresIn {
        IBuild withExpiresIn(String val);
    }

    public interface IRefreshToken {
        IExpiresIn withRefreshToken(String val);
    }

    public interface ITokenType {
        IRefreshToken withTokenType(String val);
    }

    public interface IAccessToken {
        ITokenType withAccessToken(String val);
    }

    public static final class Builder implements IExpiresIn, IRefreshToken, ITokenType, IAccessToken, IBuild {
        private String expiresIn;
        private String refreshToken;
        private String tokenType;
        private String accessToken;

        private Builder() {
        }

        @Override
        public IBuild withExpiresIn(String val) {
            expiresIn = val;
            return this;
        }

        @Override
        public IExpiresIn withRefreshToken(String val) {
            refreshToken = val;
            return this;
        }

        @Override
        public IRefreshToken withTokenType(String val) {
            tokenType = val;
            return this;
        }

        @Override
        public ITokenType withAccessToken(String val) {
            accessToken = val;
            return this;
        }

        public Token build() {
            return new Token(this);
        }
    }
}

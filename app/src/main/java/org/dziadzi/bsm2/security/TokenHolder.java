package org.dziadzi.bsm2.security;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

/**
 * Created by DELL on 2016-05-19.
 */
public class TokenHolder {


    public static final String TOKEN_STORAGE = "token_storage";

    private final Context context;
    private final SharedPreferences preferences;

    public TokenHolder(Context context) {
        this.context=context;
        this.preferences = context.getSharedPreferences(TOKEN_STORAGE, Context.MODE_PRIVATE);
    }


    public void store(Token token){

        preferences.edit().putString(token.getRefreshToken(),token.getAccessToken()).commit();
        preferences.edit().putString(Base64.encodeToString(token.getRefreshToken().getBytes(), Base64.NO_WRAP),token.getTokenType()).commit();

    }

    public void eraseToken(String token){
        preferences.edit().remove(token).commit();
        preferences.edit().remove(Base64.encodeToString(token.getBytes(), Base64.NO_WRAP)).commit();

    }
    public Token getToken(String accesToken){
        return Token.builder().withAccessToken(preferences.getString(accesToken,null))
                .withTokenType(null)
                .withRefreshToken(accesToken)
                .withExpiresIn(null).build();
    }

    public boolean hasToken(String accesToken) {
        return preferences.contains(accesToken);
    }
}
